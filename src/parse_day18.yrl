Nonterminals E.
Terminals '+' '*' '(' ')' integer.
Rootsymbol E.
E -> E '+' E: {op_add, '$1', '$3'}.
E -> E '*' E: {op_mult, '$1', '$3'}.
E -> '(' E ')': '$2'.
E -> integer: '$1'.
Left 100 '+'.
Left 100 '*'.
