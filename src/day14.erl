-module(day14).

-export([p1/0,p2/0]).

-ifdef(TEST).
-export([floating/1]).
-endif.

-include("helpers.hrl").

-define(re_split_opts(ReturnType_),[{return,ReturnType_},trim]).
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").

parse(<<"mask",_/binary>>=M) ->
    [_,Mask] = re:split(M, "([X01]+)",?re_split_opts(list)),
    {_,Rs} = lists:foldl(fun($X, {C_,Rs_}) ->
				 {C_+1,[{C_,$X}|Rs_]};
			    (V,{C_,Rs_}) ->
				 {C_+1,[{C_,V-$0}|Rs_]}
			 end,
			 {0,[]},
			 lists:reverse(Mask)),
    Rs;
parse(<<"mem",_/binary>>=O) ->
    [_,L,_,V] = re:split(O, "(\\d+)",?re_split_opts(binary)),
    {?bin_to_integer(L),?bin_to_integer(V)}.

process(Pred,Data) ->
    process(Pred,Data,none, #{}).

process(_,[],_,Rs) -> Rs;
process(Pred,[M|T],_,Rs) when is_list(M) ->
    process(Pred,T,M,Rs);
process(Pred,[H|T],M,Rs) ->
    process(Pred,T,M,Pred(H,M,Rs)).

value_decoder({L,V},M,Rs) ->
    Vn = lists:foldl(fun mask_bit/2, V, M),
    case Rs of
	#{L := _} -> Rs#{L := Vn};
	_ -> Rs#{L => Vn}
    end.

mask_bit({Bp,Bm},V) ->
    case (V bsr Bp) band 1 of
	Bm -> V;
	_ -> Mask = (1 bsl Bp),
	     case Bm of
		 1 -> V bor Mask;
		 0 -> V bxor Mask;
		 $X -> V
	     end
    end.

mask_with_floating(V,M) ->
    Vi = lists:foldl(fun({_,F},Acc) when F =:= 0 orelse F =:= $X -> Acc;
			({P,1}, Acc) ->
			     Acc bor (1 bsl P)
		     end, V, M),
    lists:map(fun(Fl) ->
		      lists:foldl(fun mask_bit/2,Vi,Fl)
	      end, floating([P || {P, $X} <- M])).

floating(L) ->
    floating(L,0,1 bsl length(L),[]).

floating(_,Max,Max,Rs) -> Rs;
floating(L,C,Max,Rs) ->
    {Fl,_} = lists:mapfoldl(fun(P, Acc) ->
				    {{P, Acc band 1}, Acc bsr 1}
			    end, C, L),
    floating(L,C+1,Max,[Fl|Rs]).

memory_decoder({L,V},M,Rs) ->
    lists:foldl(fun(Li,Acc) ->
			case Acc of
			    #{Li :=_} -> Acc#{Li := V};
			    _ -> Acc#{Li => V}
			end
		end, Rs,mask_with_floating(L,M)).

decode_memory(Pred) ->
    Input = lists:map(fun parse/1, ?input_as_bin_list(?INPUT)),
    lists:sum([N || {_,N} <- maps:to_list(process(Pred, Input))]).

p1() ->
    decode_memory(fun value_decoder/3).

p2() ->
    decode_memory(fun memory_decoder/3).
