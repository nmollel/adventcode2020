-module(day04).

-export([p1/0,p2/0]).

-include("helpers.hrl").

-define(INPUT, atom_to_list(?MODULE) ++ ".txt").
-define(ID_FIELDS, [<<"byr">>,<<"iyr">>,<<"eyr">>,<<"hgt">>,<<"hcl">>,<<"ecl">>,
		    <<"pid">>, <<"cid">>]).
-define(FIELDS_MAP, maps:from_list([{K, false}
				    || K <- ?ID_FIELDS])).
-define(HAIR_COLORS, [<<"amb">>,<<"blu">>,<<"brn">>,<<"gry">>,<<"grn">>,
		      <<"hzl">>,<<"oth">>]).
-define(split_cred(Cred@), binary:split(Cred@, [<<"\s">>,<<"\n">>], [global, trim])).

%% Private functions
is_field_present(Cred) ->
    Fields = ?split_cred(Cred),
    lists:foldl(fun(<<V:3/binary,_/binary>>, Map) ->
			Map#{V := true}
				end,
				?FIELDS_MAP,
				Fields).

is_valid(Cred) ->
    PresentFields = is_field_present(Cred),
    lists:all(fun(V) -> V end, maps:values(PresentFields)) orelse
	case [K || {K, false} <- maps:to_list(PresentFields)] of
	    [<<"cid">>] -> true;
	    _ -> false
	end.

%% @doc same as is_valid but performs field data validation
is_valid_with_correct_data(Cred) ->
    PresentFields = is_field_present(Cred),
    AllPresent = lists:all(fun(V) -> V end, maps:values(PresentFields)),
    (AllPresent andalso correct_data_format(Cred)) orelse
	case [K || {K, false} <- maps:to_list(PresentFields)] of
	    [<<"cid">>] -> correct_data_format(Cred);
	    _ -> false
	end.

correct_data_format(Cred) ->
    Fields = ?split_cred(Cred),
    lists:all(fun field_format/1, Fields).

field_format(<<"byr:",Y:4/binary>>) ->
    year_in_range(Y,{1920,2002});
field_format(<<"iyr:",Y:4/binary>>) ->
    year_in_range(Y,{2010,2020});
field_format(<<"eyr:",Y:4/binary>>) ->
    year_in_range(Y,{2020,2030});
field_format(<<"hgt:",V/binary>>) ->
    case binary:part(V, {byte_size(V), -2}) of
	<<"cm">> -> height_in_range(V, <<"cm">>, {150, 193});
	<<"in">> -> height_in_range(V, <<"in">>, {59, 76});
	_ -> false
    end;
field_format(<<"hcl:#",C:6/binary>>) ->
    Pred = fun(Char) ->
		   ?is_alpha_lower(Char) orelse ?is_num_char(Char)
	   end,
    lists:all(Pred, binary:bin_to_list(C));
field_format(<<"ecl:",C:3/binary>>) ->
    lists:any(fun(Color) -> C =:= Color end, ?HAIR_COLORS);
field_format(<<"pid:",Pid:9/binary>>) ->
    lists:all(fun(Num) -> ?is_num_char(Num) end,
	      binary:bin_to_list(Pid));
field_format(<<"cid",_/binary>>) -> true;
field_format(_) -> false.

year_in_range(Y, {Min, Max}) ->
    Year = list_to_integer(binary_to_list(Y)),
    Min =< Year andalso Year =< Max.

height_in_range(V, Unit, {Min, Max}) ->
    [L] = binary:split(V, Unit, [global,trim]),
    N = list_to_integer(binary_to_list(L)),
    Min =< N andalso N =< Max.

%% Solutions
num_of_valid_entries(Pred) ->
    Input = adventcode2020:bin_input(?INPUT),
    Entries = binary:split(Input, <<"\n\n">>, [global, trim]),
    ValidEntries = lists:filter(Pred, Entries),
    length(ValidEntries).

p1() ->
    num_of_valid_entries(fun is_valid/1).

p2() ->
    num_of_valid_entries(fun is_valid_with_correct_data/1).
