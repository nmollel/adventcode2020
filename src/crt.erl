-module(crt).

-export([crt/1]).

crt(In) ->
    [{N1,A1}|T] = lists:reverse(lists:keysort(1, In)),
    crt(T,A1,N1).

crt([],A,_) -> A;
crt([{Ni,Ai}|T]=In,A,N) ->
    Rem = A rem Ni,
    if  Rem =:= Ai -> crt(T,A,N * Ni);
	Rem =/= Ai -> crt(In, A + N, N)
    end.
