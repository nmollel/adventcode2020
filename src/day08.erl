-module(day08).

-export([p1/0,p2/0]).

-include("helpers.hrl").
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").
-define(is_prefix(S_,P_),
	begin case string:prefix(S_, P_) of
		  nomatch -> false;
		  _ -> true
	      end
	end).

instructions(Input) ->
    instructions(Input,0,#{}).

instructions([],_,Map) -> Map;
instructions([H|T],Index,Map) ->
    instructions(T, Index+1, Map#{Index=>H}).

execute(Program) ->
    execute(Program, 0, 0, []).

execute(Program, Acc, PC, Visited) ->
    case lists:search(fun(V) -> V =:= PC end, Visited) of
	{value,_} -> {loop, Acc};
	_ ->  TerminalPC = maps:size(Program),
	      case Program of
		      #{PC := Inst} ->
			  {NAcc,NPC} = execute_instruction(
					 string:lexemes(Inst, "\s"),
					 Acc, PC),
			  execute(Program, NAcc, NPC, [PC|Visited]);
		  _ when PC =:=  TerminalPC -> {ok, Acc};
		  _ -> {invalid, Acc}
	      end
    end.

execute_instruction(["nop"|_],Acc,PC) -> {Acc, PC+1};
execute_instruction(["acc",S],Acc,PC) ->
    {D,_} = string:to_integer(S),
    {Acc + D, PC + 1};
execute_instruction(["jmp",S],Acc,PC) ->
    {D,_} = string:to_integer(S),
    {Acc, PC + D};
execute_instruction(_,Acc,PC) -> {Acc, PC}.

find_fix(_, []) -> failed;
find_fix(Program, [{K, V}|T]) ->
    Inst = swap_instruction(string:lexemes(V, "\s")),
    case execute(Program#{K := Inst}) of
	{ok, Acc} -> Acc;
	_ -> find_fix(Program, T)
    end.

swap_instruction(["nop",R]) ->
    string:join(["jmp",R], "\s");
swap_instruction(["jmp",R]) ->
    string:join(["nop",R], "\s");
swap_instruction(_) -> {error, unknown_inst}.

get_program() ->
    Input = ?input_as_list(?INPUT),
    instructions(Input).

p1() ->
    Program = get_program(),
    execute(Program).

p2() ->
    Program = get_program(),
    SearchSpace = maps:filter(fun(_K,V) ->
				      ?is_prefix(V,"nop") orelse
					  ?is_prefix(V,"jmp")
			      end, Program),
    find_fix(Program,maps:to_list(SearchSpace)).
