-module(day07).

-export([p1/0,p2/0]).

-include("helpers.hrl").
-define(SPLIT_RE,"bag[s\,\.]*|contain").
-define(SHINY_GOLD, "shiny gold").
%% -define(NO_OTHER,"no other").
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").

graph_of_rules(Rules) ->
    lists:foldl(fun add_rule/2, digraph:new(), Rules).

add_rule([H|T],Graph) ->
    case digraph:vertex(Graph, H) of
	{V, _ } -> add_rule(T,Graph,V);
	_ -> add_rule(T,Graph,digraph:add_vertex(Graph, H))
    end.
add_rule([],Graph,_) -> Graph;
add_rule([[D|Rem]|T],Graph,V) when ?is_num_char(D) ->
    add_edge(string:trim(Rem),Graph,V,D - $0),
    add_rule(T,Graph,V);
add_rule([H|T],Graph,V) ->
    add_edge(H,Graph,V,no_other),
    add_rule(T,Graph,V).

add_edge(BagColor,Graph,V,Label) ->
    case digraph:vertex(Graph, BagColor) of
	{Cv, _} -> digraph:add_edge(Graph, V, Cv, Label);
	_ -> digraph:add_edge(Graph,V,digraph:add_vertex(Graph,BagColor),Label)
    end.

count_bags([],_) -> 0;
count_bags([H|T],Graph) ->
    Edges = digraph:out_edges(Graph, H),
    Counts = [begin case digraph:edge(Graph, E) of
			{_,_,_,no_other} -> 0;
			{_,_,N,L} -> L + (L * count_bags([N],Graph))
		    end
	      end || E <- Edges],
    lists:sum(Counts) + count_bags(T,Graph).

problem_graph() ->
    Input = ?input_as_list(?INPUT),
    Rules = lists:map(fun(S) ->
			      Parts = re:split(S, ?SPLIT_RE,
					       [{return,list},trim]),
			      [string:trim(P) || P <- Parts, P =/= "\s"]
		      end, Input),
    graph_of_rules(Rules).

p1() ->
    Graph = problem_graph(),
    Contains = [digraph:get_path(Graph, V, ?SHINY_GOLD) ||
		   V <- digraph:vertices(Graph)],
    length([P || P <- Contains, P =/= false]).

p2() ->
    Graph = problem_graph(),
    count_bags([?SHINY_GOLD],Graph).
