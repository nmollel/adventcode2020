-module(day09).

-export([p1/0,p2/0]).

-include("helpers.hrl").
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").
-define(PREAMBLE, 25).

%% @doc validates that the sequence matches preamble + message rule
%% or returns the first number that fails
validate_sequence([_|T] = Seq, PLen) ->
    case lists:split(PLen, Seq) of
	{_, []} -> ok;
	{Preamble, [NH|_]} ->
	    Check = [{X,Y} || X <- Preamble, Y <- Preamble -- [X],
			      X + Y =:= NH],
	    case Check of
		[] -> NH;
		[_|_] -> validate_sequence(T,PLen)
	    end
    end.

find_sum_set([H|T],Target,Seq) ->
    Sum = lists:sum(NextSeq = [H|Seq]),
    if Sum =:= Target -> NextSeq;
       Sum > Target -> case find_sum_set(Shift = lists:droplast(NextSeq),
					 Target) of
			    [_] -> find_sum_set(T,Target, Shift);
			    ShiftSub -> ShiftSub
			end;
       Sum < Target -> find_sum_set(T,Target, NextSeq)
    end;
find_sum_set([],_,_) -> failed.

find_sum_set([_]=Rs,_) -> Rs;
find_sum_set([H|_] = Rs, Target) ->
    Sum = lists:sum(Rs),
    if Sum =:= Target -> Rs;
       Sum > Target -> find_sum_set(lists:droplast(Rs), Target);
       Sum < Target -> [H] % my way to bail out
    end.

input() ->
    [list_to_integer(N) || N <- ?input_as_list(?INPUT)].

p1() ->
    Input = input(),
    validate_sequence(Input,?PREAMBLE).

p2() ->
    {Input, Target} = {input(), p1()},
    Seq = find_sum_set(Input, Target, []),
    lists:min(Seq) + lists:max(Seq).
