-module(day05).

-export([p1/0,p2/0]).

-define(INPUT,atom_to_list(?MODULE) ++ ".txt").
-define(ROWS, 128).
-define(COLUMNS, 8).

find_seat_id(<<RowCd:7/binary,ColCd:3/binary>>) ->
    Row = decode_row(RowCd),
    Col = decode_column(ColCd),
    {Row, Col, Row * 8 + Col}.

map_row_codes_region_codes(<<>>) -> [];
map_row_codes_region_codes(<<"F",Rest/binary>>) ->
    [lower|map_row_codes_region_codes(Rest)];
map_row_codes_region_codes(<<"B",Rest/binary>>) ->
    [upper|map_row_codes_region_codes(Rest)].

map_column_codes_region_codes(<<>>) -> [];
map_column_codes_region_codes(<<"L",Rest/binary>>) ->
    [lower|map_column_codes_region_codes(Rest)];
map_column_codes_region_codes(<<"R",Rest/binary>>) ->
    [upper|map_column_codes_region_codes(Rest)].

decode_column(ColCodes) ->
    RegionCodes = map_column_codes_region_codes(ColCodes),
    decode_region(RegionCodes,{0, ?COLUMNS -1}).

decode_row(RowCd) ->
    RegionCodes = map_row_codes_region_codes(RowCd),
    decode_region(RegionCodes, {0, ?ROWS - 1}).

decode_region([lower],{Rmin,_}) -> Rmin;
decode_region([upper],{_,Rmax}) -> Rmax;
decode_region([lower|Rest], {Rmin,Rmax}) ->
    Mid = floor((Rmax-Rmin)/2),
    decode_region(Rest,{Rmin, Rmin + Mid});
decode_region([upper|Rest], {Rmin,Rmax}) ->
    Mid = floor((Rmax-Rmin)/2),
    decode_region(Rest,{Rmin + Mid + 1, Rmax}).

find_seat([]) -> {error, no_seat};
find_seat([_]) -> find_seat([]);
find_seat([{_,_,CId},{_,_,NId}=N|T]) ->
    case NId - CId of
	2 -> CId + 1;
	_ -> find_seat([N|T])
    end.

input() ->
    Input = adventcode2020:bin_input(?INPUT),
    binary:split(Input, <<"\n">>,[global,trim]).

p1() ->
    Entries = input(),
    lists:max([Id || {_, _, Id} <-
			 [find_seat_id(Entry) || Entry <- Entries]]).

p2() ->
    Entries = input(),
    Seats = lists:ukeysort(3, [find_seat_id(Entry) || Entry <- Entries]),
    find_seat(Seats).
