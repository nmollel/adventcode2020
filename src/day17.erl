-module(day17).

-export([p1/0,p2/0]).

-include("helpers.hrl").
-define(neighbor_offsets(N_),
	begin
	    L_=[-1,0,1],
	    case N_ of
		3 ->
		    [{X_,Y_,Z_} || X_ <- L_, Y_ <- L_, Z_ <- L_] -- [{0,0,0}];
		4 ->
		    [{X_,Y_,Z_,U_} || X_ <- L_, Y_ <- L_, Z_ <- L_, U_ <- L_] -- [{0,0,0,0}]
	    end
	end).
-define(ACTIVE,$#).
-define(CYCLES,6).
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").

parse_into_map(List,N) ->
    AdditionalCords = case N of
			  3 -> [0];
			  4 -> [0,0]
		      end,
    lists:map(fun(P) ->
		      list_to_tuple(lists:append(P, AdditionalCords))
	      end,parse_into_map(List,0,0,[])).

parse_into_map([],_,_,Map) -> Map;
parse_into_map([[]|T],_,Y,Map) ->
    parse_into_map(T,0,Y+1,Map);
parse_into_map([[?ACTIVE|Lt]|T],X,Y,Map) ->
    parse_into_map([Lt|T],X+1,Y,[[X,Y]|Map]);
parse_into_map([[_|Lt]|T],X,Y,Map) ->
    parse_into_map([Lt|T],X+1,Y,Map).

life(List) ->
    next_generation(0,List).

next_generation(?CYCLES,Rs) -> Rs;
next_generation(N,Current) ->
    Next = maps:fold(fun(Key,3,Acc) -> [Key|Acc];
			(Key,2,Acc) ->
			     case lists:member(Key, Current) of
				 true -> [Key|Acc];
				 _ -> Acc
			     end;
			(_,_,Acc) -> Acc
		     end,[],neighbors(Current)),
    next_generation(N+1,Next).

neighbors(List) ->
    neighbors(List,#{}).

neighbors([],Map) -> Map;
neighbors([H|T],Map) ->
    Neighbors = lists:foldl(fun(Offset,Acc) ->
				    P = offset(H,Offset),
				    case Acc of
					#{P := V} -> Acc#{P := V+1};
					_ -> Acc#{P => 1}
				    end
			    end,Map,?neighbor_offsets(size(H))),
    neighbors(T,Neighbors).

%% @doc thought of generalized handling of this which is possible but involves
%% too much mapping between lists and tuples and settled for the quick and dirty
%% addition for the 3 and 4 dimension values
offset({X,Y,Z},{Xo,Yo,Zo}) -> {X+Xo,Y+Yo,Z+Zo};
offset({X,Y,Z,U},{Xo,Yo,Zo,Uo}) -> {X+Xo,Y+Yo,Z+Zo,U+Uo}.

p1() ->
    length(life(parse_into_map(?input_as_list(?INPUT),3))).

p2() ->
    length(life(parse_into_map(?input_as_list(?INPUT),4))).
