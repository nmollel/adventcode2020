-module(day06).

-export([p1/0,p2/0]).

-include("helpers.hrl").

-define(INPUT,atom_to_list(?MODULE) ++ ".txt").
-define(answers_to_list(G_),
	binary:bin_to_list(binary:replace(G_,
					  ?BIN_NEW_LINE,
					  <<"">>,
					  [global]))).

count_unique_questions(Group) ->
    Answers = ?answers_to_list(Group),
    if length(Answers) > 1 ->
	    UniqueAnswers = sets:from_list(Answers),
	    sets:size(UniqueAnswers);
       true -> 1
    end.

count_shared_answers(Group) ->
    case binary:split(Group, ?BIN_NEW_LINE, ?BIN_SPLIT_OPTS) of
	[A] -> length(binary:bin_to_list(A));
	Answers -> Map = answers_to_map(Group),
		   SharedAnswers = maps:filter(fun(_K,V) ->
						       V =:= length(Answers)
					       end,
					       Map),
		   length(maps:keys(SharedAnswers))
    end.

answers_to_map(Group) ->
    Answers = ?answers_to_list(Group),
    answers_to_map(Answers, #{}).

answers_to_map([], Map) -> Map;
answers_to_map([H|T], Map) ->
    case Map of
	#{H := V} -> answers_to_map(T, Map#{H := V + 1});
	_ -> answers_to_map(T, Map#{H => 1})
    end.

sum_of_answer_counts(Pred) ->
    Input = adventcode2020:bin_input(?INPUT),
    Entries = binary:split(Input, ?BIN_BLANK_LINE, ?BIN_SPLIT_OPTS),
    lists:sum([Pred(E) || E <- Entries]).

p1() ->
    sum_of_answer_counts(fun count_unique_questions/1).

p2() ->
    sum_of_answer_counts(fun count_shared_answers/1).
