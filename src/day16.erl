-module(day16).

-export([p1/0,p2/0]).
-compile({inline,[input/0]}).

-include("helpers.hrl").
-define(BOUNDS_RE,"([\-\\d]+)").
-define(FIELD_RE, "([a-zA-Z\s]+):").
-define(INPUT, atom_to_list(?MODULE) ++ ".txt").

parse_spec(Spec) ->
    Lines = binary:split(Spec,?BIN_NEW_LINE,?BIN_SPLIT_OPTS),
    Parsed = [begin
		  Re = ?FIELD_RE ++ "|" ++ ?BOUNDS_RE,
		  Match = lists:flatten(re:split(L, Re,
						 [trim,group])),
		  lists:filter(fun(<<>>) -> false;
				  (<<" ">>) -> false;
				  (<<" or ">>) -> false;
				  (_) -> true
			       end, Match)
	      end
	      || L <- Lines],
    lists:foldl(fun([K,LBounds,UBounds], Map) ->
			Map#{K => [parse_bounds(LBounds),
				   parse_bounds(UBounds)]}
		end, #{}, Parsed).

parse_bounds(B) ->
    [L,U] = binary:split(B, <<"-">>,?BIN_SPLIT_OPTS),
    {?bin_to_integer(L),?bin_to_integer(U)}.

parse_tickets(Tickets) ->
    [_|Ts] = binary:split(Tickets,?BIN_NEW_LINE,?BIN_SPLIT_OPTS),
    [begin
	 Tkt = binary:split(T, <<",">>,
			    ?BIN_SPLIT_OPTS),
	 [?bin_to_integer(Tf) || Tf <- Tkt]
     end
     || T <- Ts].

%% @doc the premise is that if value matches any bound, then we will terminate
%% the checks without having to check all bounds.
valid_field_value(Fv, Guards) ->
    lists:any(fun({L,U}) ->
		      U >= Fv andalso L =< Fv
	      end,
	      lists:flatten(maps:values(Guards))).

order_of_fields([F|_] = Tickets,Guards) ->
    order_of_fields(Tickets,maps:to_list(Guards),
		    {lists:seq(1, length(F)), []},[]).

order_of_fields(_,_,{[],[]},Rs) ->
    [R || {_, R} <- lists:usort(Rs)];
order_of_fields(Tickets,Guards,{[],[_|_]=Pending},Rs) ->
    order_of_fields(Tickets,Guards,{Pending,[]},Rs);
order_of_fields(Tickets,Guards,{[Ih|It],P},Rs) ->
    case find_bounds([lists:nth(Ih, Tk) || Tk <- Tickets],Guards) of
	[Bs] ->
	    order_of_fields(Tickets,Guards -- [Bs], {It,P}, [{Ih, Bs}|Rs]);
	_ -> order_of_fields(Tickets,Guards,{It,[Ih|P]},Rs)
    end.

find_bounds([],Guards) -> Guards;
find_bounds([V|T], Guards) ->
    G = lists:filter(fun({_,[{Ll,Lu},{Ul,Uu}]}) ->
			     Ll =< V andalso Lu >= V orelse Ul =< V andalso Uu >=V
		     end,Guards),
    find_bounds(T,G).

input() ->
    binary:split(adventcode2020:bin_input(?INPUT),?BIN_BLANK_LINE,?BIN_SPLIT_OPTS).

p1() ->
    [Spec,_Ticket,Others] = input(),
    Guards = parse_spec(Spec),
    Input = lists:flatten(parse_tickets(Others)),
    lists:sum(lists:filter(fun(Fv) -> not valid_field_value(Fv,Guards) end, Input)).

p2() ->
    [Spec,Ticket,Others] = input(),
    Guards = parse_spec(Spec),
    Pred = fun(Fv) -> not valid_field_value(Fv, Guards) end,
    Valid_Others = lists:filter(fun(T) ->
					not lists:any(Pred, T)
				end, parse_tickets(Others)),
    [T] = parse_tickets(Ticket),
    Rs = lists:zip(order_of_fields(Valid_Others,Guards),T),
    lists:foldl(fun({{<<"departure",_/binary>>, _}, V}, Acc) ->
			V * Acc;
		   (_,Acc) -> Acc
		end, 1, Rs).
