-module(day12).

-export([p1/0,p2/0]).

-include("helpers.hrl").

-define(RIGHT_MAP, maps:from_list([{north,east},{east,south},{south,west},
				   {west,north}])).
-define(LEFT_MAP, maps:from_list([{north,west},{west,south},{south,east},
				  {east,north}])).
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").

action(<<$N>>) -> north;
action(<<$S>>) -> south;
action(<<$E>>) -> east;
action(<<$W>>) -> west;
action(<<$L>>) -> left;
action(<<$R>>) -> right;
action(<<$F>>) -> forward;
action(_) -> {error,unknown_action}.

process(Actions) ->
    process(Actions,{0,0},east).

process([],Pos,_) -> Pos;
process([<<A:1/binary,V/binary>>|T],Pos,Heading) ->
    {NexPos,NewHeading} = move(action(A),
			       ?bin_to_integer(V),
			       Pos,Heading),
    process(T,NexPos,NewHeading).

move(forward,V,{X,Y},east) ->
    {{X+V,Y}, east};
move(forward,V,{X,Y},west) ->
    {{X-V,Y}, west};
move(forward,V,{X,Y},north) ->
    {{X,Y+V},north};
move(forward,V,{X,Y},south) ->
    {{X,Y-V},south};
move(right,A,Pos,Heading) ->
    {Pos,translate(right,A,Heading)};
move(left,A,Pos,Heading) ->
    {Pos,translate(left,A,Heading)};
move(Dir,V,Pos,Heading) ->
    {P1,_} = move(forward,V,Pos,Dir),
    {P1,Heading}.

translate(right,A,Heading) ->
    translate(A,Heading,?RIGHT_MAP);
translate(left,A,Heading) ->
    translate(A,Heading,?LEFT_MAP);
translate(0,Heading,_) -> Heading;
translate(A,Heading,Map) when is_integer(A) ->
    #{Heading := H} = Map,
    translate(A - 90, H, Map).

process_with_waypoint(Input) ->
    process_with_waypoint(Input,{0,0},{10,1}).

process_with_waypoint([],Pos,_) -> Pos;
process_with_waypoint([<<A:1/binary,V/binary>>|T],Pos,Waypoint) ->
    Action = action(A),
    case lists:member(Action, [north,east,south,west]) of
	true -> {NextWayPoint, _NextDir} = move(forward,?bin_to_integer(V),
						Waypoint,Action),
		process_with_waypoint(T,Pos,NextWayPoint);
	_ -> {NextPos, NextWayPoint} = move_with_waypoint(Action,
							  ?bin_to_integer(V),
							  Pos,Waypoint),
	     process_with_waypoint(T,NextPos,NextWayPoint)
    end.

move_with_waypoint(left,Angle,Pos,Waypoint) ->
    {Pos, rotate(Angle,Waypoint)};		% counter-clockwise
move_with_waypoint(right,Angle,Pos,Waypoint) ->
    {Pos, rotate(-Angle,Waypoint)};		% clockwise
move_with_waypoint(forward,V,{X,Y},{Wx,Wy}=Waypoint) ->
    {{X + Wx * V, Y + Wy * V}, Waypoint}.

%% @doc rotation matrix implementation as.
%% https://en.wikipedia.org/wiki/Rotation_matrix
%% sine and cosine compute values in radians
%% radians = degrees * pi /180
rotate(Angle,{X,Y}) ->
    Radians = Angle * math:pi() / 180,
    {Xn,Yn} = {X * math:cos(Radians) - Y * math:sin(Radians),
	       X * math:sin(Radians) + Y * math:cos(Radians)},
    {round(Xn), round(Yn)}.

manhattan_distance(NavigationProc) ->
    Input = ?input_as_bin_list(?INPUT),
    {X,Y} = NavigationProc(Input),
    abs(X) + abs(Y).

p1() ->
    manhattan_distance(fun process/1).

p2() ->
    manhattan_distance(fun process_with_waypoint/1).
