-module(day18).

-export([p1/0,p1_2/0,p2/0]).

-include("helpers.hrl").
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").

tokenize(Line) ->
    tokenize(Line,<<>>).

tokenize(<<>>,<<>>) ->
    [];
tokenize(<<>>,Token) ->
    [Token];
tokenize(<<"\s",Rest/binary>>,<<>>) ->
    tokenize(Rest,<<>>);
tokenize(<<"\s",Rest/binary>>,Token) ->
    [Token|tokenize(Rest,<<>>)];
tokenize(<<"(",Rest/binary>>,_) ->
    [open_paren|tokenize(Rest,<<>>)];
tokenize(<<")",Rest/binary>>,<<>>) ->
    [close_paren|tokenize(Rest,<<>>)];
tokenize(<<")",Rest/binary>>,Token) ->
    [Token,close_paren|tokenize(Rest,<<>>)];
tokenize(<<"*",Rest/binary>>,_) ->
    [op_mult|tokenize(Rest,<<>>)];
tokenize(<<"+",Rest/binary>>,_) ->
    [op_add|tokenize(Rest,<<>>)];
tokenize(<<V,Rest/binary>>,Token) ->
    tokenize(Rest,<<Token/binary,V>>).

%% build a parse tree from a list of tokens. all operators here has the same
%% precedence
parse([],[H|_]) -> H;
parse([Op,R|T],[L|St])
  when (Op =:= op_add orelse Op =:= op_mult) andalso is_binary(R) ->
    parse(T,[[Op,L,R]|St]);
parse([Op,open_paren|T],[L|St]) when Op =:= op_add; Op =:= op_mult ->
    {R,Tn} = parse(T,St),
    parse(Tn,[[Op,L,R]|St]);
parse([open_paren|T],S) ->
    {R,Tn} = parse(T,S),
    parse(Tn,[R|S]);
parse([close_paren|T],[H|_]) ->
    {H,T};
parse([H|T],Stack) ->
    parse(T,[H|Stack]).

%% @doc execute a parse tree
execute(V) when is_binary(V) ->
    ?bin_to_integer(V);
execute([op_add,L,R]) ->
    execute(L) + execute(R);
execute([op_mult,L,R]) ->
    execute(L) * execute(R);
execute(_) ->
    {error,undefined}.

eval({op_add, L, R}) ->
    eval(L) + eval(R);
eval({op_mult, L, R}) ->
    eval(L) * eval(R);
eval({integer,_,V}) -> V.

solve(Parser) ->
    lists:sum([begin
		   {ok, Tokens, _} = erl_scan:string(Line),
		   {ok, PTree} = Parser(Tokens),
		   eval(PTree)
	       end
	       || Line <- ?input_as_list(?INPUT)]).

p1() ->
    lists:sum([execute(parse(tokenize(Line),[]))
	       || Line <- ?input_as_bin_list(?INPUT)]).
p1_2() ->
    solve(fun parse_day18:parse/1).

p2() ->
    solve(fun parse_day18_2:parse/1).
