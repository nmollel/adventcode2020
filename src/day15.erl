-module(day15).

-export([p1/0,p2/0]).

-compile({inline,[update/3]}).

-define(INPUT, "2,0,1,9,5,19").
-define(INPUT_LIST, [list_to_integer(V) || V <- string:lexemes(?INPUT, ",")]).


recite(Input, Target) ->
    Turns = lists:zip(Input, lists:seq(1, length(Input))),
    {Lk,Lv} = lists:last(Turns),
    recite(Lv+1,Lk,maps:from_list([{K,[V]} || {K,V} <- Turns]),Target).

recite(Target,Last,Records,Target) ->
    case Records of
	#{Last := [_]} -> 0;
	#{Last := [L0,L1|_]} -> L0 -L1
    end;
recite(Turn,Last,Records,Target) ->
    NextTurn = Turn +1,
    case Records of
	#{Last := [_]} -> recite(NextTurn,0,update(0,Turn,Records),Target);
	#{Last := [L0,L1|_]} -> N = L0-L1,
				recite(NextTurn,N,update(N,Turn,Records),Target)
    end.

update(N,Turn,Records) ->
    case Records of
	#{N := [L0|_]} -> Records#{N := [Turn,L0]};
	_ -> Records#{N => [Turn]}
    end.

p1() ->
    recite(?INPUT_LIST,2020).

p2() ->
    recite(?INPUT_LIST,30000000).
