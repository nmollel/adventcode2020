-module(day19).

-export([p1/0,p2/0]).

-include("helpers.hrl").
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").

match([],[],_) -> true;
match([],_,_) -> false;
match(_,[],_) -> false;
match([H|T],[V|Mt]=Msg,Map) ->
    case Map of
	#{H := {terminal, [V]}} -> match(T,Mt,Map);
	#{H := {terminal, _}} -> false;
	#{H := {R1,R2}} -> match(lists:append(R1, T), Msg, Map) orelse match(lists:append(R2, T),Msg,Map);
	#{H := List} -> match(lists:append(List, T),Msg,Map);
	_ -> false
    end.

get_rule_map_and_messages() ->
    [Rules,Messages] = ?bin_split(adventcode2020:bin_input(?INPUT),
				  ?BIN_BLANK_LINE),
    RL = [begin
	      {ok,Tokens,_} = erl_scan:string(binary_to_list(Rule)),
	      {ok,P} = parse_day19:parse(Tokens),
	      P
	  end
	  || Rule <- ?bin_split(Rules,?BIN_NEW_LINE)],
    Map = maps:from_list(RL),
    {Map,Messages}.

solution(Map,Messages) ->
    TopRule = maps:get(0, Map),
    Rs = lists:filter(fun(Msg) -> match(TopRule, binary_to_list(Msg), Map) end,
		      ?bin_split(Messages,?BIN_NEW_LINE)),
    length(Rs).

p1() ->
    {Map,Messages} = get_rule_map_and_messages(),
    solution(Map,Messages).

p2() ->
    {Map,Messages} = get_rule_map_and_messages(),
    NewRules = Map#{8 := {[42],[42,8]},11 := {[42,31],[42,11,31]}},
    solution(NewRules,Messages).
