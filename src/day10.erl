-module(day10).

-export([p1/0,p2/0]).

-ifdef(TEST).
-export([arrangements/2]).
-endif.

-include("helpers.hrl").
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").
-define(TABLE_NAME, arrangements).

jolt_diff([_],[Ones,Threes]) -> [Ones, Threes];
jolt_diff([N0,N1|T],[Ones, Threes]) ->
    Rest = [N1|T],
    case N1 - N0 of
	1 -> jolt_diff(Rest,[Ones + 1, Threes]);
	3 -> jolt_diff(Rest, [Ones, Threes + 1]);
	_ -> unexpected
    end.

init_cache() ->
    case ets:info(?TABLE_NAME) of
	undefined -> ets:new(?TABLE_NAME, [named_table]);
	_ -> ok
    end.

arrangements([F|_]=L,Prev) when F - Prev > 3 ->
    ets:insert(?TABLE_NAME, {{L,Prev},0}),
    0;
arrangements([F|T]=L,Prev) ->
    init_cache(),
    case ets:lookup(?TABLE_NAME, {L,Prev}) of
	[] -> Rs = case T of
		       [] -> 1;
		       _ -> arrangements(T,F) + arrangements(T,Prev)
		   end,
	      ets:insert(?TABLE_NAME, {{L,Prev},Rs}),
	      Rs;
	[{_,V}] -> V
    end.

p1() ->
    Input = lists:sort([list_to_integer(N) ||
			   N <- ?input_as_list(?INPUT)]),
    lists:foldl(fun(N, Acc) -> N * Acc end, 1,
		jolt_diff([0|Input ++ [lists:last(Input) + 3]],
			  [0,0])).

p2() ->
    Input = lists:sort([list_to_integer(N) ||
			   N <- ?input_as_list(?INPUT)]),
    arrangements(Input,0).
