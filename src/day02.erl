-module(day02).

-export([p1/0,p2/0]).

-define(INPUT, atom_to_list(?MODULE) ++ ".txt").

valid_pass([Policy,[V|_],Pass]) ->
    Chars = [C@ || C@ <- Pass, C@ =:= V],
    [Min,Max] = [list_to_integer(S@) || S@ <- string:lexemes(Policy, "-")],
    Len = length(Chars),
    Min =< Len andalso Len =< Max.

valid_pass_pos_policy([Policy,[V|_],Pass]) ->
    [P1,P2] = [list_to_integer(S@) || S@ <- string:lexemes(Policy, "-")],
    InP1 = lists:nth(P1, Pass) =:= V,
    InP2 = lists:nth(P2, Pass) =:= V,
    (InP1 andalso not InP2) orelse (not InP1 andalso InP2).

filter_pass(FilterFun) ->
    Input = adventcode2020:input(?INPUT),
    Passwords = lists:map(fun(S@) -> string:lexemes(S@, "\s") end,
			  string:lexemes(Input, "\n")),
    lists:filter(FilterFun, Passwords).

p1() ->
    ValidPass = filter_pass(fun valid_pass/1),
    length(ValidPass).

p2() ->
    ValidPass = filter_pass(fun valid_pass_pos_policy/1),
    length(ValidPass).
