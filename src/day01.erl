-module(day01).

-export([p1/0,p2/0]).

-ifdef(TEST).
-export([entries_sum/2]).
-endif.

-define(INPUT,atom_to_list(?MODULE) ++ ".txt").
-define(input_to_list(Input),
	[list_to_integer(N@) || N@ <- string:lexemes(Input, "\n")]).
-define(SUM, 2020).

entries_sum(Input, N) ->
    Entries = ?input_to_list(Input),
    DuplicatedRs = case N of
		       2 -> [lists:sort([X,Y]) || X <- Entries, Y <- Entries -- [X],
						  X + Y == ?SUM];
		       3 -> [lists:sort([X,Y, Z]) || X <- Entries, Y <- Entries -- [X],
						     Z <- Entries -- [X, Y], X + Y + Z == ?SUM];
		       _ -> {error, undefined}
		   end,
    [Rs] = lists:usort(DuplicatedRs),
    lists:foldl(fun(N@,P) -> N@ * P end, 1, Rs).


p1() ->
    Input = adventcode2020:input(?INPUT),
    entries_sum(Input, 2).

p2() ->
    Input = adventcode2020:input(?INPUT),
    entries_sum(Input, 3).
