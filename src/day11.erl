-module(day11).

-export([p1/0,p2/0]).

-ifdef(TEST).
-export([count_first_reachable/2]).
-endif.

-include("helpers.hrl").
-define(DIRECTIONS,[{0,-1},{0,1},{-1,0},{1,0},
		    {-1,-1},{1,-1},{-1,1},{1,1}]).
-define(DIRECTION_NAMES, [up,down,left,right,d_up_left,d_up_right,d_down_left,
			  d_down_right]).
-define(EMPTY,$L).
-define(OCCUPIED,$#).
-define(FLOOR,$.).

-define(INPUT, atom_to_list(?MODULE) ++ ".txt").

mutate(Policy,Map) ->
    case mutate(Policy,Map,{0,0},0,#{}) of
	{M, 0} -> M;
	{M, _} -> mutate(Policy,M)
    end.

mutate(Policy,Map,P,Count,NextMap) ->
    case Map of
	#{P := ?FLOOR} -> mutate(Policy,Map,next(P,Map),Count,
				 NextMap#{P => ?FLOOR});
	#{P := _} -> {M, NCount} = update(Policy,P,Map,Count, NextMap),
		     mutate(Policy,Map, next(P,Map), NCount, M);
	_ -> {NextMap, Count}
    end.

update(Policy,P,Map,Count,NextMap) ->
    {Pred,Threshold} = case Policy of
			   tolerant -> {fun count_first_reachable/2, 5};
			   _ -> {fun count_occupied/2, 4}
		       end,
    case Map of
	#{P := ?EMPTY} -> case Pred(P,Map) of
			      0 -> {NextMap#{P => ?OCCUPIED}, Count + 1};
			      _ -> {NextMap#{P => ?EMPTY}, Count}
			  end;
	#{P := ?OCCUPIED} -> Occupied = Pred(P,Map),
			     if Occupied >= Threshold ->
				     {NextMap#{P => ?EMPTY}, Count + 1};
				Occupied < Threshold ->
				     {NextMap#{P => ?OCCUPIED}, Count}
			     end
    end.

count_occupied({X,Y},Map) ->
    Pos = lists:map(fun({Xc,Yc}) -> {X + Xc, Y + Yc} end, ?DIRECTIONS),
    lists:foldl(fun(P, Acc) ->
			case Map of
			    #{P := ?OCCUPIED} -> Acc + 1;
			    _ -> Acc
			end
		end, 0, Pos).

count_occupied(Dir,{X,Y},Map, DirMap) ->
    #{Dir := {Dx,Dy}} = DirMap,
    P = {X + Dx, Y + Dy},
    case Map of
	#{P := ?OCCUPIED} -> 1;
	#{P := ?EMPTY} -> 0;
	#{P := _} -> count_occupied(Dir,P,Map, DirMap);
	_ -> 0
    end.

count_first_reachable(P,Map) ->
    DirMap = maps:from_list(lists:zip(?DIRECTION_NAMES, ?DIRECTIONS)),
    lists:sum([count_occupied(D,P,Map,DirMap) || D <- ?DIRECTION_NAMES]).

next({X,Y},Map) ->
    NextR = {X + 1, Y},
    case Map of
	#{NextR := _} -> NextR;
	_ -> {0, Y + 1}
    end.

count_mutations(CountPolicy) ->
    List = ?input_as_list(?INPUT),
    Map = adventcode2020:create_map(List),
    length([P || {P, ?OCCUPIED} <- maps:to_list(mutate(CountPolicy, Map))]).

p1() ->
    count_mutations(default).

p2() ->
    count_mutations(tolerant).
