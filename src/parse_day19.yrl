Nonterminals rule value name sub_rule.
Terminals ':' '|' string integer.
Rootsymbol rule.

rule -> name ':' value: {'$1',{terminal,'$3'}}.
rule -> name ':' sub_rule: {'$1','$3'}.
rule -> name ':' sub_rule '|' sub_rule: {'$1',{'$3','$5'}}.
sub_rule -> name sub_rule: ['$1'|'$2'].
sub_rule -> name: ['$1'].
name -> integer: value_of('$1').
value -> string: value_of('$1').

Erlang code.
value_of({_,_,V}) -> V.
