-module(adventcode2020).

-export([input/1, bin_input/1, create_map/1]).

bin_input(Filename) ->
    Priv = code:priv_dir(?MODULE),
    File = filename:join(Priv, Filename),
    {ok, Bin } = file:read_file(File),
    Bin.

input(Data) ->
    Bin = bin_input(Data),
    unicode:characters_to_list(Bin).

%% X -> horizontal, Y -> vertical
create_map(List) ->
    create_map(List, 0, 0, #{}).

create_map([], _, _, Map) -> Map;
create_map([[]|T], _, Y, Map) ->
    create_map(T, 0, Y+1, Map);
create_map([[V|T]|Lt], X, Y, Map) ->
    create_map([T|Lt], X+1, Y, Map#{{X,Y} => V}).
