-module(day03).

-export([p1/0,p2/0]).

-include("helpers.hrl").

-define(INPUT, atom_to_list(?MODULE) ++ ".txt").
-define(SLOPES, [{1,1}, {3,1}, {5, 1}, {7, 1}, {1, 2}]).

traverse_map(Map, Dim, Slope) ->
    traverse_map(Map, Dim, Slope, {0,0}, 0).

traverse_map(Map, {Xmax,Ymax} = Dim, {L, D} = Slope, {X, Y} = P,
	     Trees) ->
    case Map of
	#{{X+L, Y+D} := V} ->
	    NewP = {X+L, Y+D},
	    case V of
		$. -> traverse_map(Map,Dim, Slope, NewP, Trees);
		$# -> traverse_map(Map, Dim, Slope, NewP, Trees + 1);
		_ -> {error, uknown_char}
	    end;
	_ -> if Y + 1 =:= Ymax -> Trees;
		true ->
		     NewMap = transpose_map(Map, Xmax),
		     traverse_map(NewMap,Dim,Slope, P,Trees)
	     end
    end.

transpose_map(Map,Xmax) ->
    maps:from_list([{{X + Xmax, Y}, V} || {{X,Y}, V} <- maps:to_list(Map)]).

puzzle_map() ->
    Lists = ?input_as_list(?INPUT),
    Dim = {length(hd(Lists)), length(Lists)},
    {adventcode2020:create_map(Lists), Dim}.

p1() ->
    {Map, Dim} = puzzle_map(),
    traverse_map(Map, Dim, {3,1}).

p2() ->
    {Map, Dim} = puzzle_map(),
    Trees = [traverse_map(Map, Dim, S) || S <- ?SLOPES],
    lists:foldl(fun(T, P) -> T * P end, 1, Trees).
