-module(day13).

-export([p1/0,p2/0]).

-ifdef(TEST).
-export([id_map/1,earliest_timestamp/1]).
-endif.

-include("helpers.hrl").
-define(OUT_OF_SERVICE, <<"x">>).
-define(INPUT,atom_to_list(?MODULE) ++ ".txt").

parse_ids(BinIDs) ->
    IDs = lists:filter(fun(E) -> E =/= ?OUT_OF_SERVICE end,
		       binary:split(BinIDs, <<",">>,?BIN_SPLIT_OPTS)),
    lists:map(fun(E) -> ?bin_to_integer(E) end, IDs).

earliest_bus(TimeStamp,IDs) ->
    case lists:search(fun(N) -> TimeStamp rem N =:= 0 end, IDs) of
	{value, V} -> {TimeStamp, V};
	_ -> earliest_bus(TimeStamp+1,IDs)
    end.

earliest_timestamp([{ID,Rem}|T]) ->
    earliest_timestamp(T,Rem,ID).

%% @doc Chinese remainder theorem implementation
earliest_timestamp([],V,_) -> V;
earliest_timestamp([{Ni, Ai}|T]=IDs,V,Inc) ->
    Rem = V rem Ni,
    if Rem =:= Ai -> earliest_timestamp(T,V,Inc * Ni);
       Rem =/= Ai -> earliest_timestamp(IDs, V + Inc, Inc)
    end.

id_map(BinIDs) ->
    id_map(binary:split(BinIDs, <<",">>,?BIN_SPLIT_OPTS),0,[]).

id_map([],_,Map) -> lists:reverse(lists:keysort(1, Map));
id_map([?OUT_OF_SERVICE|T],Offset,Map) ->
    id_map(T,Offset+1,Map);
id_map([V|T],Offset,Map) ->
    N = ?bin_to_integer(V),
    Rem = case -Offset rem N of
	      0 -> 0;
	      Other -> Other + N
	  end,
    id_map(T,Offset+1,[{N, Rem}|Map]).

p1() ->
    [BinTimestap,BinIDs] = ?input_as_bin_list(?INPUT),
    {Time, ID} = earliest_bus(TimeStamp = ?bin_to_integer(BinTimestap),
			      parse_ids(BinIDs)),
    (Time - TimeStamp) * ID.

p2() ->
    [_,BinIDs] = ?input_as_bin_list(?INPUT),
    earliest_timestamp(id_map(BinIDs)).
