-module(day13_SUITE).

-compile([export_all, nowarn_export_all]).

-include_lib("stdlib/include/assert.hrl").
-include_lib("common_test/include/ct.hrl").

-define(INPUT, [{"7,13,x,x,59,x,31,19", 1068781},
		{"17,x,13,19",3417},
		{"67,x,7,59,61",779210},
		{"67,7,59,61",754018},
		{"67,7,x,59,61",1261476},
		{"1789,37,47,1889",1202161486}]).

all() ->
    [test_earliest_timestamp].

test_earliest_timestamp(_Config) ->
    lists:foreach(fun({Input,Expected}) ->
			  IDMap = day13:id_map(binary:list_to_bin(Input)),
			  Rs = day13:earliest_timestamp(IDMap),
			  ?assertEqual(Expected, Rs)
		  end, ?INPUT).
