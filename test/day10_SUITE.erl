-module(day10_SUITE).

-compile([export_all, nowarn_export_all]).

-include_lib("stdlib/include/assert.hrl").
-include_lib("common_test/include/ct.hrl").

-define(INPUT,[{[3, 6, 9, 12],1},
	       {[3, 6, 9, 13],0},
	       {[1, 2, 3, 4],7}]).

all() ->
    [test_arrangements].

test_arrangements(_Config) ->
    lists:foreach(fun({Input,Expected}) ->
			  Rs = day10:arrangements(Input, 0),
			  ?assertEqual(Expected, Rs)
		  end, ?INPUT).
