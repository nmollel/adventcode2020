-module(crt_SUITE).

-compile([export_all, nowarn_export_all]).

-include_lib("stdlib/include/assert.hrl").
-include_lib("common_test/include/ct.hrl").

%% Input as {N,A} where x = A (mod N)
-define(INPUT, [{[{3,1},{4,2},{5,3}],58},
		{[{3,2},{5,3},{7,2}],23},
		{[{3,0},{4,3},{5,4}],39}]).

all() ->
    [test_crt].

test_crt(_Config) ->
    lists:foreach(fun({In,Expected}) ->
			  Rs = crt:crt(In),
			  ?assertEqual(Expected,Rs)
		  end, ?INPUT).
