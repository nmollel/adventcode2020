-module(day11_SUITE).

-compile([export_all, nowarn_export_all]).

-include_lib("stdlib/include/assert.hrl").
-include_lib("common_test/include/ct.hrl").

-define(INPUT, {".......#.\n"
		"...#.....\n"
		".#.......\n"
		".........\n"
		"..#L....#\n"
		"....#....\n"
		".........\n"
		"#........\n"
		"...#.....\n",
		{3,4},8}).

all() ->
    [test_count_first_reachable].

test_count_first_reachable(_Config) ->
    {Input, P, Expected} = ?INPUT,
    List = string:lexemes(Input, "\n"),
    C = day11:count_first_reachable(P,adventcode2020:create_map(List)),
    ?assertEqual(Expected,C).
