-module(day01_SUITE).

-compile([export_all, nowarn_export_all]).

-include_lib("stdlib/include/assert.hrl").
-include_lib("common_test/include/ct.hrl").

-define(INPUT,
	"1721\n"
	"979\n"
	"366\n"
	"299\n"
	"675\n"
	"1456\n").

-define(TWO_INPUT, {?INPUT, 514579}).
-define(THREE_INPUT, {?INPUT, 241861950}).

all() ->
    [test_two_entries_sum, test_three_entries_sum].

test_two_entries_sum(_Config) ->
    {Input, Expected} = ?TWO_INPUT,
    Rs = day01:entries_sum(Input, 2),
    ?assertEqual(Expected, Rs).

test_three_entries_sum(_Config) ->
    {Input, Expected} = ?THREE_INPUT,
    Rs = day01:entries_sum(Input, 3),
    ?assertEqual(Expected, Rs).
