-define(input_as_list(Filename@),
	begin
	    Input@ = adventcode2020:input(Filename@),
	    string:lexemes(Input@, "\n")
	end).

-define(input_as_bin_list(Filename@),
	begin
	    Input_ = adventcode2020:bin_input(Filename@),
	    ?bin_split(Input_,?BIN_NEW_LINE)
	end).

-define(bin_to_integer(V_), list_to_integer(binary:bin_to_list(V_))).
-define(bin_split(Bin_,Pattern_), binary:split(Bin_,Pattern_,?BIN_SPLIT_OPTS)).

%% the comparisons here are done in reverse to short circuit and not do the
%% lower check if the higher failed.
-define(is_num_char(Num_), $9 >= Num_ andalso Num_ >= $0).
-define(is_alpha_lower(Char_), $z >= Char_ andalso Char_ >= $a).
-define(is_alpha_uppper(Char_), $Z >= Char_ andalso Char_ >= $A).
-define(is_alpha_numeric(Char_),
	?is_num_char(Char_) orelse
	?is_alpha_lower(Char_) orelse
	?is_alpha_upper(Char_)).

-define(BIN_BLANK_LINE,<<"\n\n">>).
-define(BIN_NEW_LINE,<<"\n">>).
-define(BIN_SPLIT_OPTS, [global,trim]).
